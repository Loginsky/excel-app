<?php

namespace App\Imports;

use App\Models\Producer;
use App\Models\Product;
use App\Models\Rubric;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Collection;

/**
 * Class ProductsImport
 * @package App\Imports
 */
class ProductsImport implements
    ToCollection,
    SkipsOnError,
    WithValidation,
    SkipsOnFailure,
    WithChunkReading,
    ShouldQueue,
    WithEvents,
    WithHeadingRow
{
    use Importable, SkipsErrors, SkipsFailures, RegistersEventListeners;

    private $duplicatedEntry = false;

    /**
    * @param Collection $rows
    *
    * @return Collection
    */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            /*
             * $row
             * [
                "rubrika" => "Электроинструменты"
                "pod_rubrika" => "Перфораторы Bosch"
                "kategoriya_tovara" => "Перфораторы Bosch для дома"
                "proizvoditel" => "Bosch"
                "naimenovanie_tovara" => "Перфоратор Bosch PBH 2100 RE"
                "kod_modeli_artikul_proizvoditelya" => "06033A9320"
                "opisanie_tovara" => "Эргономичный, компактный, легкий (2,2 кг), Slimline, 550-ваттный перфоратор Bosch PBH 2100 RE, совершая до 2300 об/мин и до 5800 уд/мин, выполняет сверление с у ▶"
                "cena_rozn_grn" => 3149
                "garantiya" => 24
                "nalicie" => "есть в наличие"
                "" => null
                ]
             */
            //dd($row);

            /********************************  Fix Structure  *******************************/

            // fix common "catalog_for_test.xlsx" error - firs column-cell is empty (column: 'Рубрика')
            if (!$row["rubrika"] && $row["pod_rubrika"]) {
                $row["rubrika"] = $row["pod_rubrika"];// 'Рубрика' ("rubrika")
            }

            // Validation - First two product cells ['Рубрика', 'Под-Рубрика'] are empty
            if (!$row["rubrika"] && !$row["pod_rubrika"]) {
                continue;
                // TODO: fix breaking data in this case (possibly send to "failed_jobs")
            }

            /********************************  Store data  ********************************/

            $rubricParent   = new Rubric();
            $rubricChild    = new Rubric();
            $producer       = new Producer();
            $product        = new Product();

            /*** Rubric ***/
            $rubricParentFound = $rubricParent->where(['name' => $row["rubrika"]])->first();
            if (is_null($rubricParentFound)) {
                $rubricParent->name = $row["rubrika"];
                $rubricParent->save();
            }

            if (!is_null($rubricParentFound) && is_null($rubricChild->where(['name' => $row["pod_rubrika"]])->first())) {
                $rubricChild->name = $row["pod_rubrika"];
                $rubricChild->parent_id = $rubricParentFound->id;
                $rubricChild->save();
            }

            /*** Product Producer ***/
            $producerFound = $producer->where(['name' => $row["proizvoditel"]])->first();
            if (is_null($producerFound)) {
                $producer->name = $row["proizvoditel"];
                $producer->save();
            }

            /*** Product ***/
            $productFound = $product->where(['name' => $row["kod_modeli_artikul_proizvoditelya"]])->first();
            if (is_null($productFound)) {
                $product->category = $row["kategoriya_tovara"];
                $product->name = $row["naimenovanie_tovara"];
                $product->code = $row["kod_modeli_artikul_proizvoditelya"];
                $product->description = $row["opisanie_tovara"];
                $product->price = $row["cena_rozn_grn"];
                $product->warranty = $row["garantiya"];
                $product->in_stock = $row["nalicie"];
                // Relations:
                $product->producer_id = is_null($producer->id) ?? $producerFound->id;
                $product->rubric_parent_id = is_null($rubricParent->id) ?? $rubricParentFound->id;
                $product->save();
            }

        }

        self::afterImport();
    }


    public function rules(): array
    {
        return [
            '*.5' => ['unique:App\Models\Product,code', 'max:255']
        ];
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public static function afterImport(AfterImport $event) : array
    {
        $arrTotalRows = $event->getReader()->getTotalRows();
        foreach($arrTotalRows as $amount) {
            Log::alert("The amount products from first sheet was: " . $amount);
            break;
        }
        return $arrTotalRows;
    }

    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function(ImportFailed $event) {
                $exception = $event->getException();

                if($exception->getCode() == 23000) {
                    // Duplicated "$product->code"
                    $this->duplicatedEntry = true;
                } else {
                    dd($event->getException());
                }
                //$this->notify(new ImportHasFailedNotification);
            }
        ];
    }

    /**
     * Validation errors
     * @param Failure ...$failures
     */
    public function onFailure(Failure ...$failures)
    {
        dd($failures);
    }


}
