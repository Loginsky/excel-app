<?php

namespace App\Http\Controllers;

use App\Imports\ProductsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


/**
 * Class ProductsImportController
 * @package App\Http\Controllers\
 */
class ProductsImportController extends Controller
{
    public $layout = 'layouts.default';

    /**
     * Display Import page.
     *
     * @return
     */
    public function show()
    {
        return view('products.import');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(Request $request)
    {
        // Validation
        $validator = Validator::make(
            [
                'file'      => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
            ],
            [
                'file'      => 'required',
                'extension' => 'required|in:xlsx'
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        // Save file
        $file = $request->file('file')->store('import');

        // Import products
        $import = new ProductsImport;
        $import->queue($file);

        return back()->with('status', 'Products added via queue !');
    }

    /**
     * WARNING: https://stackoverflow.com/questions/49473098/call-to-undefined-method-maatwebsite-excel-excelload
     * Same issue happened with "->selectSheetsByIndex(0)" needed for 2+ chunk in queue when only first chunk was added to db
     */
}
