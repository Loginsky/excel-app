<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;

/**
 * @mixin Builder
 */
class Product extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category',
        'name',
        'code',
        'description',
        'price',
        'warranty',
        'in_stock',
        // Relations:
        'producer_id',
        'rubric_parent_id',
    ];

    public function producer()
    {
        return $this->belongsTo(Producer::class);
    }

    public function rubric()
    {
        return $this->belongsTo(Rubric::class, 'foreign_key', 'rubric_parent_id');
    }
}
