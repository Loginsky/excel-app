<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->string('category');
            $table->string('name');
            $table->string('code');
            $table->string('description', 2048);
            $table->integer('price');
            $table->string('warranty');
            $table->string('in_stock')->index('in_stock_boolean_index')
                ->default('есть в наличие');
            $table->integer('rubric_parent_id')->index('rubric_parent_id_index');
            $table->integer('producer_id')->index('producer_id_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
