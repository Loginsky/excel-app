<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Excel Import

- Library used - https://docs.laravel-excel.com/3.1
- Laravel 8+

Task description:
- організувати мінімальне використання оперативної пам'яті. Візьміть
  до уваги, що розмір файлу може бути яким завгодно завеликим;
- max_execution_time на один запит - не більше 10 sec;
- вивести лічильник імпортованих записів;
- структура EXEL-файлу порушена, спробуйте знайти рішення як уникнути
  змішування даних;
- при імпорті перевіряти дублі записів і не заносити записи, що вже
  присутні, виводити повідомлення - скільки записів не занесено і
  причину;
- реалізувати валідацію файлу, що завантажується: розширення,
  Mime-тип, розмір файлу;
- створити Laravel custom rule який буде обчислювати максимально
  допустимий сервером розмір файлу для завантаження;
- створіть окремі таблиці товарів та категорій в БД. Побудуйте зв'язки.
